package be.kdg.prog12.m5.view.stats;

import be.kdg.prog12.m5.model.LudoGame;
import javafx.scene.chart.XYChart;

public class StatsPresenter {
    private final LudoGame model;
    private final StatsView view;

    public StatsPresenter(LudoGame model, StatsView view) {
        this.model = model;
        this.view = view;

        addEventHandlers();
        updateView();
    }

    private void addEventHandlers() {
        // No event handlers here in this demo.
    }

    private void updateView() {
        // TODO: IMPORTANT: pull this data from the model
        // TODO: Model could return a List< <Difficulty, Double> >
        // TODO: probably better to let the model return a 'Statistics' object

        // Here in the presenter, the data is going to be poured into a JavaFX-specific 'model'.
        XYChart.Series<String, Number> series1
         = new XYChart.Series<>();
        // TODO: this is going to be a loop!
        series1.getData().add(new XYChart.Data<>("Easy" /* enum --> call 'name()' perhaps, ... capitalize */, 90.0));
        series1.getData().add(new XYChart.Data<>("Medium", 80.0));
        series1.getData().add(new XYChart.Data<>("Hard", 60.0));
        series1.getData().add(new XYChart.Data<>("Extreme", 10.0));
        series1.setName("January");

        XYChart.Series<String, Number> series2
                = new XYChart.Series<>();
        // TODO: this is going to be a loop!
        series2.getData().add(new XYChart.Data<>("Easy", 91.0));
        series2.getData().add(new XYChart.Data<>("Medium", 79.0));
        series2.getData().add(new XYChart.Data<>("Hard", 62.5));
        series2.getData().add(new XYChart.Data<>("Extreme", 5.0));
        series2.setName("February");

        view.getAverageScoresByDifficulty()
                .getData().add(series1);
        view.getAverageScoresByDifficulty()
                .getData().add(series2);
    }
}
