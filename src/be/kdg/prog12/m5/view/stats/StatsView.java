package be.kdg.prog12.m5.view.stats;

import javafx.scene.chart.AreaChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.layout.BorderPane;

public class StatsView extends BorderPane {
    private AreaChart<String, Number> averageScoresByDifficulty;

    public StatsView() {
        initialiseNodes();
        layoutNodes();
    }

    private void initialiseNodes() {
        CategoryAxis xAxis = new CategoryAxis();
        xAxis.setLabel("Difficulty");

        NumberAxis yAxis = new NumberAxis(0.0, 100.0, 10.0);
        yAxis.setLabel("Average Score");

        averageScoresByDifficulty = new AreaChart<>(xAxis, yAxis);
    }

    private void layoutNodes() {
        setCenter(averageScoresByDifficulty);
    }

    AreaChart<String, Number> getAverageScoresByDifficulty() {
        return averageScoresByDifficulty;
    }
}
