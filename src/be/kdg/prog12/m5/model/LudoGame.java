package be.kdg.prog12.m5.model;

import java.util.List;

public class LudoGame {
    /*
    board / tiles
    players
    stats
    highscores
    ...
    */

    public List<Tile> getHighlightedTiles() {
        // Hardcoded for now

        // TODO: Tile objects should NOT be created at this location
        // TODO: ask the board for some data, whatever that may mean
        return List.of(new Tile(1));
    }

    // TODO: getPawnsOfPlayer(Player player)
    // TODO: getAllPawns()
    // return a list of pawns, each of which refer to a tile
    // return List<Pawn>
}
